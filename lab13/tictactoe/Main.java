package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InvalidMoveException{
        Scanner reader = new Scanner(System.in);

        Board board = new Board();

        System.out.println(board);
        while (!board.isEnded()) {

            int player = board.getCurrentPlayer();
            boolean invalidRow = false;
            int row = 0;

            do{
                System.out.println("Player "  + player + " enter row number");
                try{
                    row = Integer.valueOf(reader.nextLine());
                    System.out.println("Valid Integer");
                    invalidRow = false;
                }catch (NumberFormatException ex){
                    System.out.println("Invalid Integer");
                    invalidRow = true;
                }
            }while(invalidRow);

            /*System.out.print("Player " + player + " enter row number:");
            int row = Integer.valueOf(reader.nextLine());*/

            boolean invalidCol= false;
            int col = 0;
            do{
                System.out.print("Player " + player + " enter column number:");

                try{
                    col = Integer.valueOf(reader.nextLine());
                    System.out.println("Valid Integer");
                    invalidCol = false;
                    col = Integer.valueOf(reader.nextLine());
                }catch (NumberFormatException ex){
                    System.out.println("Invalid Integer");
                    invalidCol = true;
                }
            }while(invalidCol);

            try{
                board.move(row, col);
            }catch (InvalidMoveException ex ){
                System.out.println(board);
                System.out.println(ex.getMessage());

            }


            System.out.println(board);
        }


        reader.close();
    }


}
