public class MyDateTime{
    MyDate date;
    MyTime time;
    public MyDateTime(MyDate date, MyTime time){
        this.date = date;
        this.time = time;
    }

    public String toString(){
        return  date.toString() + " " + time.toString();

    }

    public void incrementDay(){
        date.incrementDay();
    }

    public void incrementHour(){
        int daydiff = time.incrementHour(1);
        date.incrementDay(daydiff);
    }

    public void incrementHour(int i){
        int daydiff = time.incrementHour(i);
        //date.incrementDay(daydiff);
        if(daydiff <0){
            date.decrementDay(-daydiff);

        }
        else date.incrementDay(daydiff);

    }

    public void decrementHour(int diff){

        incrementHour(-diff);
    }

    public void incrementMinute(int diff){
        int dayDiff = time.incrementMinute(diff);
        //int daydiff = time.incrementHour(i);
        //date.incrementDay(daydiff);
        if(dayDiff <0){
            date.decrementDay(-dayDiff);

        }
        else date.incrementDay(dayDiff);
    }

    public void decrementMinute(int diff){

    }


}
