package shapes3d;

import shapes2d.Square;

public class Cube extends Square {
    public Cube(int side) {
        super(side);
    }

    public int area(){
        return 6*side*side;
    }

    public int volume(){
        return side*side*side;
    }
    public String toString(){
        return "side of cube: " + side;
    }
}
