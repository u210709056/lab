import java.io.IOException;
import java.util.Scanner;

public class TicTacToe1 {

    public static void main(String[] args) throws IOException {
        //Scanner reader = new Scanner(System.in);
        char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

        boolean result_check1 = false;
        boolean result_check2 = false;

        printBoard(board);
        int i=0;
        boolean bool = false;
        while(i<9){

            result_check1 = false;
            result_check2 = false;
            int row;
            int col;

            out1:while(bool == false){
                row = RequestRow1();
                col = RequestCol1();

                bool =ControlAndReplaceInputs(row, col, board, 'X');
                if (bool == true) {
                    i++;
                    result_check1 = CheckBoardFor1(board);
                    result_check2 = CheckBoardFor2(board);
                    if (result_check1 == true) {
                        System.out.println("Player 1 won the game!");
                        return;
                    }
                    if (result_check2 == true) {
                        System.out.println("Player 2 won the game!");
                        return;
                    }
                    else break out1;
                }

            }
            //bool = false;
            out: while(bool == false){
                row = RequestRow2();
                col = RequestCol2();

                bool =ControlAndReplaceInputs(row, col, board, 'O');
                if (bool == true) {
                    i++;
                    result_check1 = CheckBoardFor1(board);
                    result_check2 = CheckBoardFor2(board);
                    if (result_check1 == true) {
                        System.out.println("Player 1 won the game!");
                        return;
                    }
                    if (result_check2 == true) {
                        System.out.println("Player 2 won the game!");
                        return;
                    }
                    else break out;
                }

            }
        }
        if(result_check1==false || result_check1==false)
            System.out.println("That games ended with a draw");
    }
    public static void printBoard(char[][] board) {
        System.out.println("    1   2   3");
        System.out.println("   -----------");
        for (int row = 0; row < 3; ++row) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < 3; ++col) {
                System.out.print("|");
                System.out.print(" " + board[row][col] + " ");
                if (col == 2)
                    System.out.print("|");

            }
            System.out.println();
            System.out.println("   -----------");
        }
    }
    public static int RequestRow1(){
        Scanner reader = new Scanner(System.in);
        System.out.print("Player 1 enter row number:");
        int row = reader.nextInt();
        return row;
    }
    public static int RequestCol1(){
        Scanner reader = new Scanner(System.in);
        System.out.print("Player 1 enter column number:");
        int col = reader.nextInt();
        return col;
    }
    public static int RequestRow2(){
        Scanner reader = new Scanner(System.in);
        System.out.print("Player 2 enter row number:");
        int row = reader.nextInt();
        return row;
    }
    public static int RequestCol2(){
        Scanner reader = new Scanner(System.in);
        System.out.print("Player 2 enter column number:");
        int col = reader.nextInt();
        return col;
    }
    public static boolean ControlAndReplaceInputs(int row, int col, char[][] board, char ch) {
        boolean bool = false;
        if ((row < 4 && 0 < row) && (col < 4 && 0 < col)) {
            if ((board[row - 1][col - 1] != 'X') && (board[row - 1][col - 1] != 'O')) {
                board[row - 1][col - 1] = ch;
                printBoard(board);
                bool = true;
            }
        }
        return bool;
    }

    public static boolean CheckBoardFor1(char[][] board) {
        boolean bool = false;
        if(board[0][0] == 'X'){
            if(board[0][1] == 'X' && (board[0][2] == 'X'))
                bool = true;
            if(board[1][1] == 'X' && (board[2][2] == 'X'))
                bool = true;
            if(board[1][0] == 'X' && (board[2][0] == 'X'))
                bool = true;
        }
        if(board[1][0] == 'X'){
            if(board[1][1] == 'X' && (board[1][2] == 'X'))
                bool = true;
        }
        if(board[2][0] == 'X'){
            if(board[2][1] == 'X' && (board[2][2] == 'X'))
                bool = true;
        }
        if(board[0][1] == 'X'){
            if(board[1][1] == 'X' && (board[2][1] == 'X'))
                bool = true;
        }
        if(board[0][2] == 'X'){
            if(board[1][2] == 'X' && (board[2][2] == 'X'))
                bool = true;
        }
        if(board[0][2] == 'X'){
            if(board[1][1] == 'X' && (board[2][0] == 'X'))
                bool = true;
        }
        return bool;
    }
    public static boolean CheckBoardFor2(char[][] board) {
        boolean bool = false;
        if(board[0][0] == 'O'){
            if(board[0][1] == 'O' && (board[0][2] == 'O'))
                bool = true;
            if(board[1][1] == 'O' && (board[2][2] == 'O'))
                bool = true;
            if(board[1][0] == 'O' && (board[2][0] == 'O'))
                bool = true;
        }
        if(board[1][0] == 'O'){
            if(board[1][1] == 'O' && (board[1][2] == 'O'))
                bool = true;
        }
        if(board[2][0] == 'O'){
            if(board[2][1] == 'O' && (board[2][2] == 'O'))
                bool = true;
        }
        if(board[0][1] == 'O'){
            if(board[1][1] == 'O' && (board[2][1] == 'O'))
                bool = true;
        }
        if(board[0][2] == 'O'){
            if(board[1][2] == 'O' && (board[2][2] == 'O'))
                bool = true;
        }
        if(board[0][2] == 'O'){
            if(board[1][1] == 'O' && (board[2][0] == 'O'))
                bool = true;
        }
        return bool;
    }
}