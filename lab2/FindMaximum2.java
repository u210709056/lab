public class FindMaximum2 {

    public static void main(String[] args){
        int value1 = 1;
        int value2 = 3;
        int result;

        boolean someCondition = true;

        result = someCondition ? value1 : value2;

        System.out.println(result);

    }
}