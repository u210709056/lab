package generics;

import java.util.ArrayList;

public class StackDemo {
    public static void main(String[] args) {
        Stack stack = new StackImpl();
        testStack(stack);

        ArrayList<Object> things = new ArrayList<>();
        things.add("doğukan");
        things.add(25);


        System.out.println(things.toString());



       /* stack.push(5);
        stack.push(6);
        stack.push(2);
        stack.push(11);
        stack.push(23);

        while(!stack.empty()){
            System.out.println(stack.pop());
        }*/
    }

    public static void testStack(Stack stack){
        stack.push(5);
        stack.push(6);
        stack.push(2);
        stack.push(11);
        stack.push(23);

        while(!stack.empty()){
            System.out.println(stack.pop());
        }

    }
}
