package generics;

public class StackImpl implements Stack {
    StackItem top;

    public void push (Object item){
        StackItem stackItem = new StackItem(item);
        stackItem.setNext(top);
        top = stackItem;
    }

    public Object pop(){
        Object item = top.getItem();
        top = top.getNext();
        return item;
    }

    public boolean empty(){
        return  top == null;
    }
}
