package generics;

public interface Stack {
    void push (Object item);
    Object pop();
    boolean empty();
}
