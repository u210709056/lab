package generics;

import java.util.ArrayList;

public class StackArrayImpl implements Stack {
    private ArrayList stack = new ArrayList();

    @Override
    public void push(Object item) {
        stack.add(item);
    }

    @Override
    public Object pop() {
        return null;
    }

    @Override
    public boolean empty() {
        return stack.size() == 0;
    }
}
