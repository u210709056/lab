import java.util.Scanner;
public class GCDLoop {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("provide first number: ");
        int number1 = input.nextInt();

        System.out.print("provide second number: ");
        int number2 = input.nextInt();

        int result = GCDLoop(number1>number2? number1:number2, number1>number2? number2:number1);
        System.out.println("GCD of these numbers: " + result);

    }
    public static int GCDLoop(int num1, int num2){
        int remainder;
        do {
            remainder = num1 % num2;
            num1 = num2;
            num2 = remainder;
        }
        while(remainder != 0);
        return num1;
    }

}